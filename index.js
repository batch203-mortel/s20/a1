let num = Number(prompt("Please input a number."));
console.log("The number you provided is " + num);

for(let i = num; i>=0; i--){
	if (i <= 50){
		break;
	}

	if( i % 10 === 0 ){
		console.log(i + " is divisible by 10. Skipping " + i)
		continue;
	}

	if(i % 5 === 0){
		console.log(i);
	}
}

let word = 'supercalifragilisticexpialidocious';
let newWord = ''
console.log(word);

for(let i = 0; i < word.length ; i++){
	if( word[i] === 'a' ||
		word[i] === 'e' ||
		word[i] === 'i' ||
		word[i] === 'o'||
		word[i] === 'u'){
		continue;
	}

	newWord += word[i];

}

console.log(newWord);